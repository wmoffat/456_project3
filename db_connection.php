<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////
  
  //function for Mysql database connection
  function connect($user, $password, $database, $port=3306){
    $mysql_link = new \mysqli(
      'localhost',
      $user,
      $password,
      $database,
      $port
    );
    $mysql_link->set_charset("utf8");

    return $mysql_link;
  }

  //function to generate UUID
  function get_uuid($mysql_link){
    $uuid = $mysql_link->query("SELECT UUID() as uuid");
    if($mysql_link->error) throw new \Exception($mysql_link->error);
    $uuid = $uuid->fetch_assoc();
    return $uuid['uuid'];
  }
?>
