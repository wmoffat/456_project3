<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////

  //include db_connection to use function in it.
  include "db_connection.php";

  //database connection
  $mysql_link = connect('456s16grp6', '456Group6', '456s16grp6');

  //session start to use values that are already stored in session variable 
  session_start();

  //if not logged-in, redirect to login.php
  if(!isset($_SESSION["username"])){
    header("Location: login.php");
  }
	
  //get user id and user id from query string
	$id = $mysql_link->real_escape_string($_GET["id"]);
	$user_id = $mysql_link->real_escape_string($_GET["user_id"]);

  //mysql query for deleting existing post.
	$delete_posts = $mysql_link->query("
  	DELETE FROM post
  	WHERE
  	  post_id = '$id'
	");

  //throw exception
	if($mysql_link->error) throw new \Exception($mysql_link->error);

  //redirect to forum.php after done operating all features.
	header('location:forum.php');
  die();

?>