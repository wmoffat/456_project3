<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////
  
	include "db_connection.php";
	session_start();

	$mysql_link = connect('456s16grp6', '456Group6', '456s16grp6');

	$post_id = $_POST['postid'];

	$blogpost = $mysql_link->real_escape_string($_POST["editedpost"]);

	$post_edit = $mysql_link->query("
      UPDATE POST 
      SET content = '$blogpost'
      WHERE post_id = '$post_id'
	   ");
    if($mysql_link->error) throw new \Exception($mysql_link->error);

    header("Location: forum.php");
    die();
?>