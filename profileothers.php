<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////

	include "db_connection.php";
	session_start();

	//a file for a default header
	include 'defaultNav.php';
	
	if(!isset($_SESSION["username"])){
		header("Location: login.php");
	}

	$mysql_link = connect('456s16grp6', '456Group6', '456s16grp6');

	  
	$user_id = $_GET["id"];

	//check double signup
	$user = $mysql_link->query("
		SELECT
		  fullname,
		  email,
		  locale,
		  gender,
		  user_img
		FROM USER
		WHERE user_id = '$user_id'
		");
	if($mysql_link->error) throw new \Exception($mysql_link->error);

	foreach ($user as $value) {
		$name = $value["fullname"];
		$email = $value["email"];
		$locale = $value["locale"];
		$gender = $value["gender"];
		$img = $value["user_img"];
	}

	if($img == "NULL" || $img == "") {
		$img = "images/default.png";
	}
	  	
?>


<!DOCTYPE html>
<head>
	<title><?php echo($name) . "'s "?>Profile - TechTalks</title>
	<!--Sources-->
		<!--BOOTSTRAP-->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<!--Custom Styles-->
		<link rel="stylesheet" href="styles.css" type="text/css"/>

	<!--JQUERY-->
		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="scripts/leanModal-modified-for-onload.js"></script> <!--MODAL SCRIPT FOR POPUPS-->
</head>
<body>

<div class="site-wrapper">
<script>
		var str ="";
      	window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '1604932423130222', // App ID
		      //channelUrl : 'http://hayageek.com/examples/oauth/facebook/oauth-javascript/channel.html', // Channel File
		      status     : true, // check login status
		      cookie     : true, // enable cookies to allow the server to access the session
		      xfbml      : true  // parse XFBML
		    });
		    
		    
			FB.Event.subscribe('auth.authResponseChange', function(response) 
			{
		 	 if (response.status === 'connected') 
		  	{
		  		//window.location = "index.php";
		  		//SUCCESS
		  		
		  		
		  	}	 
			else if (response.status === 'not_authorized') 
		    {
		    	document.getElementById("message").innerHTML +=  "<br>Failed to Connect";

				//FAILED
		    } else 
		    {
		    	window.location = "login.php";
		    }
			});	
			
		};

		
		function Logout()
		{
			FB.logout(function(){document.location = "login.php";});
		}


		(function(d){
		   	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		    if (d.getElementById(id)) {return;}
		    js = d.createElement('script'); js.id = id; js.async = true;
		    js.src = "//connect.facebook.net/en_US/all.js";
		    ref.parentNode.insertBefore(js, ref);
	    }(document));
		

    </script>
      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Purdue TechTalks</h3>
              <?php echo $navigationCode; ?>
            </div>
          </div>

          <div style="text-align:left;">
	          <ul id="team-list" style="list-style: none">
	          	<li style="font-size: 15px;"><button type="button" class="btn"><a href="forum.php" style="color:black;">Back to Forum</a></button></li>
	          	<br />
	          	<li><img id="userpic" src="<?=$img?>" class="img-circle"/><?php echo($name)."'s Profile"?></li>
	          	<br />
	          	<li><p>Name: <?=$name?></p></li>
	          	<li><p>Email: <?=$email?></p></li>
	          	<li><p>Gender: <?=$gender?></p></li>
	          	<li><p>Location: <?=$locale?></p></li>
	          	<!--<li><p id="userupdate"></p></li>-->
	          </ul>
      	  </div>

		  <div class="mastfoot">
            <div class="inner">
              <p>Trademark Purdue TechTalks. Created by: William Moffat, Youngho Lee, Ben Lamb, Keith Cercone</a> CGT 456 Spring 2016</p>
            </div>
          </div>

		</div> <!--End of cover container-->



      </div> <!--End of site wrapper inner-->

    </div> <!--End of site wrapper-->







</body>
</html>