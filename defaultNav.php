 <?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////
 
	//this file will contain a bit of default HTML in a string
	//every page that needs that header will have a php echo command to inject this HTML
	//essentially, this is the master navigation/header to edit, which will change it on all pages
	
	//there are two possible cases: user is logged in or not
	//someone who's logged in should be able to see more options in their navigation thing
	
	//if not logged-in, use a header with less options
	if(!isset($_SESSION["username"])){
		
		//generating this HTML is a little complicated simply because we need to give one of the <li> elements the "active" class, based on the URI
		
		//add the first part of the HTML
		$navigationCode = "<nav> <ul class=\"nav masthead-nav\"> <li";

		//then, in the middle of the <li> tag, perform the check
		//if the string "about" is in the current URI, it will return a non-false result, meaning it's time to insert the active tag
		if ( strpos($_SERVER["REQUEST_URI"], "about") !== false ){
			$navigationCode .= " class=\"active\"";//add the active tag
		}
		//continue building the menu
		//the next item to add is about.php, which we just checked whether or not was the active page
		$navigationCode .= "><a href=\"about.php\">About</a></li> <li";

		//perform the "active" check for the next link
		if ( strpos($_SERVER["REQUEST_URI"], "team") !== false ){
			$navigationCode .= " class=\"active\"";
		}
		$navigationCode .= "><a href=\"team.php\">Team</a></li><li";
		
		//perform the "active" check for the next link
		if ( strpos($_SERVER["REQUEST_URI"], "login") !== false ){
			$navigationCode .= " class=\"active\"";
		}
		$navigationCode .= "><a href=\"login.php\">Login</a></li> </ul> </nav>";
	}
	
	//if someone IS logged-in, then they get links to the forum, their profile, etc
	elseif(isset($_SESSION["username"])){
		
		//same as above
		
		$navigationCode = "<nav> <ul class=\"nav masthead-nav\"> <li";
		
		//perform the "active" check for the next link
		if ( strpos($_SERVER["REQUEST_URI"], "forum") !== false ){
			$navigationCode .= " class=\"active\"";
		}
		$navigationCode .= "><a href=\"forum.php\">Forum</a></li><li";
		
		//perform the "active" check for the next link
		if ( strpos($_SERVER["REQUEST_URI"], "profile") !== false ){
			$navigationCode .= " class=\"active\"";
		}
		$navigationCode .= "><a href=\"profile.php\">Profile</a></li> <li";
		
		//perform the "active" check for the next link
		if ( strpos($_SERVER["REQUEST_URI"], "about") !== false ){
			$navigationCode .= " class=\"active\"";
		}
		$navigationCode .= "><a href=\"about.php\">About</a></li> <li";
		
		//perform the "active" check for the next link
		if ( strpos($_SERVER["REQUEST_URI"], "team") !== false ){
			$navigationCode .= " class=\"active\"";
		}
		$navigationCode .= "><a href=\"team.php\">Team</a></li> <li><a href=\"#\" onclick=\"Logout();\">Logout</a></li> </ul> </nav>";
	}
?>
