<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////

	include "db_connection.php";
	session_start();

	$mysql_link = connect('456s16grp6', '456Group6', '456s16grp6');

	$post_id = get_uuid($mysql_link);
	$user_id = $_SESSION["userid"];

	$poster = $_SESSION["username"];
	
	$blogpost = $mysql_link->real_escape_string($_POST["blogpost"]);
	$userimage = $mysql_link->real_escape_string($_POST["imgurl"]);

	$blogs = $mysql_link->query("
	      INSERT INTO POST (
		        post_id,
		        user_id,
		        date_posted,
		        content,
		        poster
		      ) VALUES (
		      	'$post_id',
		        '$user_id',
		        NOW(),
		        '$blogpost',
		        '$poster'
		      )
		   ");
	if($mysql_link->error) throw new \Exception($mysql_link->error);


	$imgurl_check = $mysql_link->query("
      SELECT
      	user_img
      FROM 
      	USER
      WHERE
      	user_id = '$user_id'
	   ");
	if($mysql_link->error) throw new \Exception($mysql_link->error);

	if($imgurl_check != "" || $imgurl_check != "Null"){
		$user_info = $mysql_link->query("
      UPDATE USER 
      SET user_img = '$userimage'
      WHERE user_id = '$user_id'
	   ");
    if($mysql_link->error) throw new \Exception($mysql_link->error);
	}
  	


	header("Location: forum.php");
	die();
?>