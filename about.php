<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////
  
  session_start();
  //a file for a default header
  include 'defaultNav.php';
	
?>
<!DOCTYPE html>
<head>
	<title>About Us - TechTalks</title>

	<!--Sources-->
		<!--BOOTSTRAP-->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<!--Custom Styles-->
		<link rel="stylesheet" href="styles.css" type="text/css"/>

	<!--JQUERY-->
		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="scripts/leanModal-modified-for-onload.js"></script> <!--MODAL SCRIPT FOR POPUPS-->
</head>
<body>
	<div class="site-wrapper">
      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Purdue TechTalks</h3>
              <?php echo $navigationCode; ?>
            </div>
          </div>

          <h1 style="color:#FFD100">What is our idea?</h1>
          <p>To bring together a social network specifically for Purdue Polytechnic students. This is a 
            social hub which allows them the opportunity to communicate and share thoughts/ideas.</p>

          <h1 style="color:#2EAF9B">Why are we doing it?</h1>
          <p style="padding-bottom: 50px;">Well, we wanna be able to pass CGT 456 in all honesty. And it's really cool to develop something.</p>

		  <div class="mastfoot">
            <div class="inner">
              <p>Trademark Purdue TechTalks. Created by: William Moffat, Youngho Lee, Ben Lamb, Keith Cercone CGT 456 Spring 2016</p>
            </div>
          </div>

		</div> <!--End of cover container-->



      </div> <!--End of site wrapper inner-->

    </div> <!--End of site wrapper-->



</body>
</html>