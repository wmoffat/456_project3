<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////
  
  include "db_connection.php";

  //database connection
  $mysql_link = connect('456s16grp6', '456Group6', '456s16grp6');

  session_start();
  
  //a file for a default header
  include 'defaultNav.php';
  
  if(!isset($_SESSION["username"])){
    header("Location: login.php");
  }

  $post_edit = $_GET['id'];

  $blogcontents = $mysql_link->query("
    SELECT
      post_id,
      content,
      date_posted,
      p.user_id,
      u.fullname,
      u.user_img
    FROM POST P INNER JOIN USER U 
    ON P.user_id = U.user_id
    ORDER BY date_posted desc;
  ");
  if($mysql_link->error) throw new \Exception($mysql_link->error);

  
?>

<!DOCTYPE html>
<head>
	<title>Blog Feed - TechTalks</title>

	<!--Sources-->
		<!--BOOTSTRAP-->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<!--Custom Styles-->
		<link rel="stylesheet" href="styles.css" type="text/css"/>

	<!--JQUERY-->
		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="scripts/leanModal-modified-for-onload.js"></script> <!--MODAL SCRIPT FOR POPUPS-->
</head>
<body>
	<div class="site-wrapper">
<script>
    window.fbAsyncInit = function() {
        FB.init({
          appId      : '1604932423130222', // App ID
          //channelUrl : 'http://hayageek.com/examples/oauth/facebook/oauth-javascript/channel.html', // Channel File
          //status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true  // parse XFBML
        });
        
        
      FB.Event.subscribe('auth.authResponseChange', function(response) 
      {
       if (response.status === 'connected') 
        {
          //window.location = "index.php";
          //SUCCESS
          FB.api('/me/picture?type=normal', function(response) {

            var str=response.data.url;

            document.getElementById("imgurl").value = str;
            document.getElementById("userpic").src =str;
          });

          FB.api('/me?fields=gender,email,age_range,birthday,hometown', function(response) {
            document.getElementById("useremail").innerHTML = response.email;
            document.getElementById("usergender").innerHTML = response.gender;
            document.getElementById("userage").innerHTML = response.hometown;
          });
          
        }  
      else if (response.status === 'not_authorized') 
        {
          document.getElementById("message").innerHTML +=  "<br>Failed to Connect";

        //FAILED
        } else 
        {
          window.location = "login.php";
        }
      }); 

      
    };


    function Logout()
    {
      FB.logout(function(){document.location = "login.php";});
    }


    (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
      }(document));


</script>
      <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="inner">
              <h3 class="masthead-brand">Purdue TechTalks</h3>
              <?php echo $navigationCode; ?>
            </div>

            
            <h1 style="color:#FFD100;text-align:center; padding-top:20px;">Submit a post to the blog feed!</h1>

            <div id="blogsubmit" class="inner">
              <form method="POST" action="processpost.php" id="submitpost" style="display:block;margin:auto;text-align:center;">
                <table>
                  <tr>
                    <td><span style="font-size:12pt;color:white">What's on your mind?</span></td>
                    <td>&nbsp;</td>
                    <td><textarea form="submitpost" name="blogpost" rows="4" cols="50" style="border-radius:15px; font-size:12pt;"></textarea></td>
                  </tr>
                  <!--End of submission form-->

                  <tr style="text-align:right;"> <!--submit button-->
                    <td colspan="50">
                      <input type="hidden" name="imgurl" id="imgurl"/>
                      <button type="submit" value="Submit" style="margin-top:5px;" class="btn">Submit</button>
                    </td>
                  </tr>

                </table>

              </form>
            </div>

            <!--<?php echo $_SESSION["username"];?>-->

            <h1 style="color:#2EAF9B; text-align:center;">Blog Feed</h1>

            <div id="viewblog" class="inner">
              <table border="1">
              <?php 
                  foreach ($blogcontents as $value) { ?>
                  <tr>

                      <td style="padding:5px"><?php if($value["user_img"] == "NULL" || $value["user_img"] == ""){ ?> <img src="images/default.png" style="width: 80px; height: 80px;"> <?php } else { ?><img src="<?=$value["user_img"]?>" style="width: 80px; height: 80px;"> <?php }?> <br/> <span id="profileothers" style="color:#FFD100;">
                        <a href="profileothers.php?id=<?=$value['user_id']?>"><?=$value["fullname"]?></span></a>
                      </td>
                      <?php
                          if ($post_edit == $value["post_id"]) { ?>
                        	<form method="POST" action="processmodify.php" id="submitmodifypost" style="display:block;margin:auto;text-align:center;">
	                        	<!--<input type="hidden" name="userid" id="userid" value="<?=$value["user_id"]?>"/>-->
	                        	<td style="padding:5px; width: 100%;">
                            <textarea form="submitmodifypost" name="editedpost" id="editedpost"rows="4" cols="50" style="color:#000; border-radius:15px; font-size:12pt;"><?=$value["content"]?></textarea></td>
                      			<td style="padding:5px">
	                          	<input type="hidden" name="postid" id="postid" value="<?=$value["post_id"]?>"/>
	                            <button type="submit">
	                              <span style="color:green">Save</span>
	                            </button>
                          	</form>
                      <?php } else { ?>
                      	  	<td style="padding:5px; font:black;"><?=$value["content"]?></input></td>
                      		<td style="padding:5px">
                      <?php } ?>


                      </td>
                      <!--<td><?=$value["user_id"]?></td>-->
                  </tr>
              <?php  } ?>
              </table>
            </div>

              <div class="inner">
                <p style="color:lightgrey">Trademark Purdue TechTalks. Created by: William Moffat, Youngho Lee, Ben Lamb, Keith Cercone CGT 456 Spring 2016</p>
              </div>
      </div> <!--End of site wrapper inner-->


    </div> <!--End of site wrapper-->



</body>
</html>