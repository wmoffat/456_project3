<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////
  
  session_start();
  //a file for a default header
  include 'defaultNav.php';
  
?>

<!DOCTYPE html>
<head>
	<title>Contact us! - TechTalks</title>
	<!--Sources-->
		<!--BOOTSTRAP-->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<!--Custom Styles-->
		<link rel="stylesheet" href="styles.css" type="text/css"/>

	<!--JQUERY-->
		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="scripts/leanModal-modified-for-onload.js"></script> <!--MODAL SCRIPT FOR POPUPS-->
</head>
<body>

<div class="site-wrapper">
<script>
window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '1604932423130222', // App ID
		      //channelUrl : 'http://hayageek.com/examples/oauth/facebook/oauth-javascript/channel.html', // Channel File
		      status     : true, // check login status
		      cookie     : true, // enable cookies to allow the server to access the session
		      xfbml      : true  // parse XFBML
		    });
		    
		    
			FB.Event.subscribe('auth.authResponseChange', function(response) 
			{
		 	 if (response.status === 'connected') 
		  	{
		  		//window.location = "index.php";
		  		//SUCCESS
		  		FB.api('/me/picture?type=normal', function(response) {

				  var str=response.data.url;
			  	  document.getElementById("userpic").src =str;
			  	  	    
		    	});

		    	FB.api('/me?fields=gender,email,age_range,birthday,hometown', function(response) {
		    		document.getElementById("useremail").innerHTML = response.email;
		    		document.getElementById("usergender").innerHTML = response.gender;
		    		document.getElementById("userage").innerHTML = response.hometown;
		    	});
		  		
		  	}	 
			else if (response.status === 'not_authorized') 
		    {
		    	document.getElementById("message").innerHTML +=  "<br>Failed to Connect";

				//FAILED
		    } 
		    else 
		    {
		    	window.location = "login.php";
		    }
			});	

			FB.api('/me/picture?type=normal', function(response) {

				  var str=response.data.url;
			  	  document.getElementById("userpic").src =str;
			  	  	    
		    });
			
		};

		function Login()
		{
		
			FB.login(function(response) {
			   	if (response.authResponse) 
			   	{		
			    	//getUserInfo();
	  			} 
	  			else 
	  			{
	  	    	 console.log('User cancelled login or did not fully authorize.');
	   			}
			},{scope: 'email,user_photos,user_birthday,user_education_history,user_about_me'});
		
		
		}

		function Logout()
		{
			FB.logout(function(){document.location = "login.php";});
		}


		(function(d){
		   	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		    if (d.getElementById(id)) {return;}
		    js = d.createElement('script'); js.id = id; js.async = true;
		    js.src = "//connect.facebook.net/en_US/all.js";
		    ref.parentNode.insertBefore(js, ref);
	    }(document));
    </script>
	
      <div class="site-wrapper-inner">

        <div class="cover-container">

          
            <div class="inner">
              <h3 class="masthead-brand"><a href="forum.php">Purdue TechTalks</a></h3>
              <?php echo $navigationCode; ?>
            </div>

	          <div style="text-align:left; margin-top:10px" class="inner">
		          <ul id="team-list" style="list-style: none">
		          	<li><img src="images/max.jpg" class="img-circle"/>Max Moffat<span class="emails"> - wmoffat@purdue.edu</span></li>
		          	<li><img src="images/leo.jpg" class="img-circle"/>Youngho Lee<span class="emails"> - lee1929@purdue.edu</span></li>
		          	<li><img src="images/ben.jpg" class="img-circle"/>Ben Lamb<span class="emails"> - lambb@purdue.edu</span></li>
		          	<li><img src="images/keith.jpg" class="img-circle"/>Keith Cercone<span class="emails"> - kdcercone@gmail.com</span></li>
		          </ul>
	      	  </div>

            <div class="inner">
              <p>Trademark Purdue TechTalks. Created by: William Moffat, Youngho Lee, Ben Lamb, Keith Cercone</a> CGT 456 Spring 2016</p>
            </div>

		</div> <!--End of cover container-->



      </div> <!--End of site wrapper inner-->

    </div> <!--End of site wrapper-->






</body>
</html>