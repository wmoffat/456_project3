<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////

	session_start();
	

	//$mysql_link = connect('456s16grp6', '456Group6', '456s16grp6');

	$app_id = "1604932423130222";
	$app_secret = "bff489f7fbe8d68aa041871c223a2a59";
	$my_url = "http://cgtweb1.tech.purdue.edu/456/cgt456web1w/project3/index.php";
	
	
	function file_get_contents_curl($url) {
	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

	    $data = curl_exec($ch);
	    curl_close($ch);

	    return $data;
	}

	function get_url($url)
	{
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	    $tmp = curl_exec($ch);
	    curl_close($ch);
	    return $tmp;
	}


	
	if (isset($_REQUEST["code"])) 
	{
		$code = $_REQUEST["code"];

		$token_url = "https://graph.facebook.com/oauth/access_token?client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url) . "&client_secret=" . $app_secret . "&code=" . $code;

		$access_token = get_url($token_url);

		$graph_url = "https://graph.facebook.com/me?" . $access_token;

		$user = json_decode(get_url($graph_url));
		//print_r($user);

		$_SESSION["userid"] = $user->id;
		$_SESSION["username"] = $user->name;
		$_SESSION["useremail"] = $user->email;
		//$_SESSION["usergender"] = $user->gender;
		//$_SESSION["userage"] = $user->age_range;
		header("Location: profile.php");

	}
	else 
	{
		$dialog_url = "http://www.facebook.com/dialog/oauth?client_id=". $app_id. "&redirect_uri=".urlencode($my_url);

		echo("<script>top.location.href='".$dialog_url."'</script>");
	}
	
	
?>



