<?php
  ////////////////////////////////////////
  //
  // CGT 456 Project 3 Spring 2016
  //
  // Youngho Lee - lee1929@purdue.edu
  // Max Moffat - wmoffat@purdue.edu
  // Ben Lamb - lambb@purdue.edu
  // Keith Cercone - kdcercone@gmail.com
  //
  ////////////////////////////////////////

	session_start();
	session_destroy();

	//a file for a default header
	include 'defaultNav.php';
	
?>

<!DOCTYPE html>
<head>
	<title>Purdue TechTalks</title>

	<!--Sources-->
	<!--BOOTSTRAP-->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<!--Custom Styles-->
	<link rel="stylesheet" href="styles.css" type="text/css"/>

	<!--JQUERY-->
	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="scripts/leanModal-modified-for-onload.js"></script> <!--MODAL SCRIPT FOR POPUPS-->

</head>
<body>


	
 <div class="site-wrapper">
		

		<!--
		  Below we include the Login Button social plugin. This button uses
		  the JavaScript SDK to present a graphical Login button that triggers
		  the FB.login() function when clicked.
		-->

    	

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Purdue TechTalks</h3>
              <?php echo $navigationCode; ?>
            </div>
          </div>


          <!--Switch between navbar and page content-->


          <div class="inner cover">
            <h1 class="cover-heading" style="color:#FFD100">Share your knowledge. <span style="color:#2EAF9B">Connect.</span></h1>
            <p class="lead">Purdue TechTalks is a way for Purdue Polytechnic students to communicate, share ideas and stay updated on everything
            	tech at Purdue University.</p>
            <p class="lead ui-content">
              <a href="#" class="btn btn-default" onclick="Login();" style="width:40%; color:#3b5998; font-weight:bold;">Login with Facebook!</a>
            </p>
              <div id="fb-root"></div>
	    
			    <script>
			    	

			    	window.fbAsyncInit = function() {
			    		FB.init({
					      appId      : '1604932423130222', // App ID
					      //channelUrl : 'http://hayageek.com/examples/oauth/facebook/oauth-javascript/channel.html', // Channel File
					      status     : false, // check login status
					      cookie     : true, // enable cookies to allow the server to access the session
					      xfbml      : true  // parse XFBML
					    });
					};



					function Login()
					{
					
						FB.login(function(response) {
						   	if (response.authResponse) 
						   	{		
						    	//getUserInfo();
						    	//document.getElementById("userpic").submit()
						    	location.href = "index.php";
				  			} 
				  			else 
				  			{
				  	    		location.href = "login.php";
				   			}
						},{scope: 'email,user_photos,user_videos'});
					
					
					}

					function Logout()
					{
						FB.logout(function(){document.location = "login.php";});
					}

					(function(d){
					   	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
					    if (d.getElementById(id)) {return;}
					    js = d.createElement('script'); js.id = id; js.async = true;
					    js.src = "//connect.facebook.net/en_US/all.js";
					    ref.parentNode.insertBefore(js, ref);
				    }(document));
			    </script>

            </p>

            <br/>
            <br/>

          </div>

			<div id="status"></div>

          <div class="mastfoot">
            <div class="inner">
              <img style="margin-bottom: 20px;" src="images/Purduelogo.png" width="30%" height="auto" alt=""/>
			  <p>Trademark Purdue TechTalks. Created by: William Moffat, Youngho Lee, Ben Lamb, Keith Cercone</a> CGT 456 Spring 2016</p>
            </div>
          </div>

        </div>

      </div>

  </div>

</body>
</html>

